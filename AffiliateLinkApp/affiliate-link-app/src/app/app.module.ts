import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';

import { AppComponent } from './app.component';

import { HomeComponent } from './home/home.component';
import { ToolbarComponent } from './components/toolbar/toolbar.component';
import { FilterComponent, FilterDeleteDialog, FilterEditDialog, FilterAddDialog } from './components/filter/filter.component';
import { ListComponent, ListDialogComponent, DeleteReviewDialog, UploadReviewDialog } from './components/list/list.component';
import { FooterComponent } from './components/footer/footer.component';
import { AdminComponent } from './admin/admin.component';

import { routing } from './app.routing';
import { Router } from '@angular/router';
import { CanActivateAuthGuard, CanActivateUnauthGuard } from 'src/app/guards/guards';
import { RegionFilterComponent } from './components/region-filter/region-filter.component';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { MatImportModule } from './modules/mat-import/mat-import.module';
import { LoginComponent } from './login/login.component'
import { AuthService } from 'src/app/services/auth.service';
import { HttpClientModule } from '@angular/common/http';
import { BannerComponent } from './components/banner/banner.component';
import { MAT_DATE_LOCALE } from '@angular/material/core';
import { AddReviewDialog } from 'src/app/components/list/dialog_templates/add-dialog/add.review.dialog.cp';
import { EditReviewDialog } from 'src/app/components/list/dialog_templates/edit-review-dialog/edit.review.dialog.cp';
import { FlexLayoutModule } from '@angular/flex-layout';
import { DeferLoadDirective } from './directives/defer-load/defer-load.directive'

@NgModule({
  declarations: [
    AppComponent,
    HomeComponent,
    ToolbarComponent,
    FilterComponent,
    ListComponent,
    FooterComponent,
    AdminComponent,
    RegionFilterComponent,
    ListDialogComponent,
    LoginComponent,
    BannerComponent,
    FilterEditDialog,
    FilterDeleteDialog,
    FilterAddDialog,
    AddReviewDialog,
    EditReviewDialog,
    DeleteReviewDialog,
    UploadReviewDialog,
    DeferLoadDirective
  ],
  imports: [
    BrowserModule,
    routing,
    FormsModule,
    MatImportModule,
    ReactiveFormsModule,
    HttpClientModule,
    FlexLayoutModule
  ],
  entryComponents: [
    ListDialogComponent,
    FilterEditDialog,
    FilterDeleteDialog,
    FilterAddDialog,
    AddReviewDialog,    
    EditReviewDialog,
    DeleteReviewDialog,
    UploadReviewDialog
  ],
  providers: [
    CanActivateAuthGuard,
    CanActivateUnauthGuard,
    AuthService,
    { provide: MAT_DATE_LOCALE, useValue:'en-AU' }
  ],
  bootstrap: [AppComponent]
})
export class AppModule { }

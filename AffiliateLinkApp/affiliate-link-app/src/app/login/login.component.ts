import { Component, OnInit } from '@angular/core';
import { FormControl, Validators, FormGroupDirective, NgForm, FormGroup, FormBuilder } from '@angular/forms';
import { ErrorStateMatcher } from '@angular/material/core';
import { Router } from '@angular/router';
import { AuthService } from 'src/app/services/auth.service';

/** Error when invalid control is dirty, touched, or submitted. */
export class MyErrorStateMatcher implements ErrorStateMatcher {
  isErrorState(control: FormControl | null, form: FormGroupDirective | NgForm | null): boolean {
    const isSubmitted = form && form.submitted;
    return !!(control && control.invalid && (control.dirty || control.touched || isSubmitted));
  }
}

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  loginForm: FormGroup;
  message: string;

  constructor(
    private router: Router,
    private authService: AuthService,
    private formBuilder : FormBuilder
  ) { }

  ngOnInit() {

    this.loginForm = new FormGroup({
      email: new FormControl(),
      password: new FormControl()
    });

    (this.loginForm.controls.email as FormControl).setValidators([Validators.required, Validators.email]);
    (this.loginForm.controls.password as FormControl).setValidators([Validators.required]);

  }

  matcher = new MyErrorStateMatcher();

  get f() { return this.loginForm.controls };

  login() {

    if (this.loginForm.invalid) {
      console.log('invalid');
      return;
    }

    this.message = null;

    this.authService.login(this.f.email.value, this.f.password.value).then(
      (res: any) => {
        if (res) {
          if (res.result === 'success') {
            // reroute to admin
            this.router.navigate(['admin']);
          } else {
            console.log(res.message);
            this.message = res.message;
            this.invalidateForm();
          }
        } else {
          this.message = res;
          this.invalidateForm();
        }
      }
    );
  }

  invalidateForm() {
    this.f.email.setErrors({ required: false });
    this.f.password.setErrors({ required: false });
  }
}

import { Directive, ElementRef, AfterViewInit, Input, Renderer2, SimpleChanges } from '@angular/core';
import { forEach } from '@angular/router/src/utils/collection';
import { Review } from '../../classes/review';

@Directive({
    selector: '[appDeferLoad]'
})
export class DeferLoadDirective implements AfterViewInit {

    loaded: boolean = false;

    @Input("review")
    review: Review = null;
    @Input("wwwRoot")
    wwwRoot: string = null;
    @Input("invisibleClass")
    invisibleClass: string = "invisible";
    @Input("visibleClass")
    visibleClass: string = "visible";

    ngAfterViewInit(): void {

        this.observer = new IntersectionObserver((ev, obs) => {
            ev.forEach((v) => {
                if (v.isIntersecting) {

                    this.renderer.removeClass(this.element.nativeElement, this.invisibleClass);
                    this.renderer.addClass(this.element.nativeElement, this.visibleClass);
                    this.element.nativeElement.src = this.fullImageURL;

                    this.observer.disconnect();
                    this.loaded = true;
                    //console.log(this.src);
                    //this.element.nativeElement.src = this.src;
                    //this.element.nativeElement.src = this.src;
                    //this.renderer.removeClass(this.element.nativeElement, this.hiddenClass);
                    //this.observer.disconnect();
                }
            });

            //this.observer.disconnect();
        }, { threshold: 0 });

        this.observer.observe(this.element.nativeElement)
    }

    private observer: IntersectionObserver;

    constructor(
        private element: ElementRef,
        private renderer: Renderer2
    ) {
    }

    ngOnChanges(changes: SimpleChanges) {
        console.log(this.review);
        //console.log(changes);
        // You can also use categoryId.previousValue and 
        // categoryId.firstChange for comparing old and new values

    }

    // images
    imageReady(): boolean {
        return this.review.file && this.review.src;
    }

    // image isnt ready and review specifies imgURL
    imageProvided(): boolean {
        return !this.imageReady() && this.review.imgURL != null && this.review.imgURL != "";
    }

    imageExists(): boolean {
        return this.review.src || (this.review.imgURL != null && this.review.imgURL != "");
    }

    get fullImageURL(): string {
        if (this.imageReady()) {
            return this.review.src;
        }
        if (this.imageProvided()) {
            return this.wwwRoot + this.review.imgURL;
        }
        return "";
    }


}

export class Review {
  id: number;
  imgURL: string;
  name: string;
  oldPrice: number;
  newPrice: number;
  date: Date;
  transcript: string;
  affiliateLinks: string[][];
  regionTags: string[];
  filterTags: string[];
  file: File; // only used when adding files, null when no files to be added
  src: any; // data for file to read by img src attribute

  // determines image col and row span
  // range 0-2
  // 0 = 1 x 1
  // 1 = 2 x 1
  // 2 = 2 x 2
  importance: number; //
  dirty: boolean;
}

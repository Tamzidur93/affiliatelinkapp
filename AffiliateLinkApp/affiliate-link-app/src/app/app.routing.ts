import { Routes, RouterModule } from '@angular/router';
import { HomeComponent } from './home/home.component';
import { AdminComponent } from './admin/admin.component';
import { LoginComponent } from './login/login.component';
import { CanActivateAuthGuard, CanActivateUnauthGuard } from 'src/app/guards/guards';

const appRoutes: Routes = [
  { path: '', component: HomeComponent },
  { path: 'admin', component: AdminComponent, canActivate: [CanActivateAuthGuard] },
  { path: 'login', component: LoginComponent, canActivate: [CanActivateUnauthGuard] },
  { path: '**', redirectTo: '' }
]

export const routing = RouterModule.forRoot(appRoutes);

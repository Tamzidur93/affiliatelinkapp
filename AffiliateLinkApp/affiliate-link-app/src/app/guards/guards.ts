import { Injectable } from '@angular/core'
import { CanActivate, Router } from '@angular/router'
import { AuthService } from 'src/app/services/auth.service';
//import { AuthService } from '../services/auth.service'

@Injectable()
export class CanActivateAuthGuard implements CanActivate {

  constructor(private authService: AuthService,private router: Router) { }

  canActivate(): boolean {
    //if (this.authService.isLoggedIn()) {
    if (this.authService.isLoggedIn()) {
      return true;
    } else {
      // TODO REMOVE THIS
      // redirect to login
      this.router.navigate(['login']);
      // clear credentials
      this.authService.logout();
      return false;
    }
  }

}

@Injectable()
export class CanActivateUnauthGuard implements CanActivate {

  constructor(private authService: AuthService, private router: Router) { };

  canActivate(): boolean {
    if (this.authService.isLoggedIn()) {
      this.router.navigate(['admin']);
      return false;
    } else {
      return true;
    }
  }
}

import { Injectable } from '@angular/core';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Filter } from 'src/app/classes/filter';
import { AuthService } from 'src/app/services/auth.service';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class FilterService {

  private urlExtension: string = "filter/";
  private auth_token_key: string = 'auth_token';
  private expires_in_key: string = 'expires_in';

  constructor(private http: HttpClient, private authService: AuthService, private router: Router) { }

  public getFilters(): Promise<Filter[]> {
    return this.http.get(environment.apiUrl + this.urlExtension + 'get_filters').toPromise().then(
      (res: any) => {
        var filterList = new Array<Filter>();

        if (res.result === 'success') {

          //console.log(res.message);          
          for (let i = 0; i < res.message.length; i++) {
            let filter: Filter = {
              filterID: res.message[i].filterID,
              name: res.message[i].name
            };
            filterList.push(filter);
          }
          console.log(filterList);

        }

        return filterList;
      },
      (error) => {
        this.handleObservableError(error);
        return null;
      }
    );
  }

  public updateFilter(filter: Filter) {

    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['login']);
      return null;
    }

    return this.http.patch(
      environment.apiUrl + this.urlExtension + 'update_filter',
      {
        filterID: filter.filterID,
        name: filter.name
      },
      {
        headers:
        { 'Authorization': 'Bearer ' + this.authService.getToken() }
      }).toPromise().then(
      (res: any) => {
        var filterList = new Array<Filter>();

        if (res.result === 'success') {

          //console.log(res.message);          
          for (let i = 0; i < res.message.length; i++) {
            let filter: Filter = {
              filterID: res.message[i].filterID,
              name: res.message[i].name
            };
            filterList.push(filter);
          }
          console.log(filterList);

        }

        return filterList;
      },
      (error) => {
        this.handleObservableError(error);
        return null;
      }
      );
  }

  public deleteFilter(filter: Filter) {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['login']);
      return null;
    }

    return this.http.request(
      'delete',
      environment.apiUrl + this.urlExtension + 'delete_filter',
      {
        body: { filterID: filter.filterID },
        headers: { 'Authorization': 'Bearer ' + this.authService.getToken() }
      }).toPromise().then(
      (res: any) => {

        var filterList = new Array<Filter>();

        if (res.result === 'success') {

          //console.log(res.message);          
          for (let i = 0; i < res.message.length; i++) {
            let filter: Filter = {
              filterID: res.message[i].filterID,
              name: res.message[i].name
            };
            filterList.push(filter);
          }
          console.log(filterList);
          
        }

        return filterList;
      },
      (error) => {
        this.handleObservableError(error);
        return null;
      });
  }

  public addFilter(filter : Filter) {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['login']);
      return null;
    }

    return this.http.post(
      environment.apiUrl + this.urlExtension + 'add_filter',
      { name: filter.name },
      { headers: { 'Authorization': 'Bearer ' + this.authService.getToken() } }
    ).toPromise().then(
      (res: any) => {
        var filterList = new Array<Filter>();

        if (res.result === 'success') {

          //console.log(res.message);          
          for (let i = 0; i < res.message.length; i++) {
            let filter: Filter = {
              filterID: res.message[i].filterID,
              name: res.message[i].name
            };
            filterList.push(filter);
          }
          console.log(filterList);

        }

        return filterList;
      },
      (error) => {
        this.handleObservableError(error);
        return null;
      }
    );
  }

  handleObservableError(e: any) {
    console.log(e);
  }

}

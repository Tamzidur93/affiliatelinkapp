import { Injectable } from '@angular/core';
import { Http } from '@angular/http';
import { HttpClient } from '@angular/common/http';
import { JwtHelperService } from '@auth0/angular-jwt';
import { environment  } from '../../environments/environment';

@Injectable({
  providedIn: 'root'
})
export class AuthService {

  private urlExtension: string = "auth/";
  private auth_token_key: string = 'auth_token';
  private expires_in_key: string = 'expires_in';

  constructor(private http: HttpClient) { }

  public login(email: string, password: string) {
    return this.http.post(environment.apiUrl + this.urlExtension + "login", { email: email, password: password }).toPromise().then(
      (res: any) => {
        // json response
        if (res.result === 'success') {
          localStorage.setItem(this.auth_token_key, JSON.parse(res.message).auth_token);
          localStorage.setItem(this.expires_in_key, JSON.parse(res.message).expires_in);
        }
        return res;
      },
      (error: any) => {
        this.handleObservableError(error);
        return null;
      }
    )
  }

  public logout() {
    localStorage.removeItem(this.auth_token_key);
    localStorage.removeItem(this.expires_in_key);
  };

  public isLoggedIn(): boolean {
    var token = this.getToken();

    if (!token) return false;

    const date = this.getTokenExpirationDate(token);

    if (date === undefined) return false;

    return (date.valueOf() > new Date().valueOf());
  }

  public getToken(): string {
    return localStorage.getItem(this.auth_token_key);
  }

  private getExpiration(): string {
    return localStorage.getItem(this.expires_in_key);
  }

  private getTokenExpirationDate(token: string) {
    var jwtHelper = new JwtHelperService();
    const decoded = jwtHelper.decodeToken(token);

    const date = new Date(0);

    date.setUTCSeconds(decoded.exp);

    return date;
  }

  private handleObservableError(obj: any) {
    console.log(obj);
  }
}

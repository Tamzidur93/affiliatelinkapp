import { Injectable } from '@angular/core';
import { Router } from '@angular/router';
import { HttpClient } from '@angular/common/http';
import { environment } from '../../environments/environment';
import { Review } from 'src/app/classes/review';
import { AuthService } from 'src/app/services/auth.service';

@Injectable({
  providedIn: 'root'
})
export class ReviewService {

  private urlExtention: string = "review/";

  constructor(
    private http: HttpClient,
    private router: Router,
    private authService: AuthService
  ) {
  }

  public getReviews() {
    return this.http.get(environment.apiUrl + this.urlExtention + 'get_reviews').toPromise().then(
      (res: any) => {

        var reviews = new Array<Review>();

        if (res.result === 'success') {

          console.log(res.message);

          for (let i = 0; i < res.message.length; i++) {
            var servResp = res.message[i];

            var affiliateLinks: string[][] = [];
            var afflinksRes = (servResp.affiliateLinks as string).split(',');

            for (let i = 0; i < afflinksRes.length; i += 2) {
              affiliateLinks.push([afflinksRes[i], afflinksRes[i + 1]]);
            }

            let review: Review = {
              id: servResp.reviewID,
              affiliateLinks: affiliateLinks,
              date: new Date(Date.parse(servResp.date)),
              dirty: false,
              file: null,
              filterTags: servResp.filterTags ? servResp.filterTags.split(',') : [],
              imgURL: servResp.imgURL,
              name: servResp.name,
              newPrice: servResp.newPrice,
              oldPrice: servResp.oldPrice,
              regionTags: null,
              src: null,
              transcript: servResp.transcript,
              importance: servResp.importance ? servResp.importance : 0
            };

            reviews.push(review);
          }
        }

        return reviews;

      },
      (error) => {
        this.handleObservableError(error);
      }
    );
  }

  public addReview(review: Review, file: File) {

    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['login']);
      return null
    }

    return this.PostReview(review).then(
      (res: number) => {
        if (res && res > -1) {

          if (file) {
            return this.addReviewImage(file, res).then(
              (res: any) => {
                return res.result;
              },
              (error) => {
                this.handleObservableError(error);
              }
            );
          } else {
            return this.getReviews().then(
              (res: any) => {
                return res.result;
              },
              (error) => {
                this.handleObservableError(error);
              }
            )
          }
        }
      }
    )
  }

  private PostReview(review: Review) {

    var affiliateLinksCSV = this.AffiliateLinksToCSV(review.affiliateLinks);

    return this.http.post(
      environment.apiUrl + this.urlExtention + 'add_review',
      {
        name: review.name,
        oldPrice: review.oldPrice,
        newPrice: review.newPrice,
        date: review.date.toUTCString(),
        transcript: review.transcript,
        affiliatelinks: affiliateLinksCSV,
        filterTags: review.filterTags ? review.filterTags.join(',') : null
      },
      { headers: { "Authorization": "Bearer " + this.authService.getToken() } }
    ).toPromise().then(
      (res: any) => {
        console.log(res);
        if (res.result === 'success') {
          let reviewID = res.message;
          return reviewID;
        }
        return -1;
      },
      (error: any) => {
        this.handleObservableError(error);
        return null;
      }
    )
  }

  public updateReview(review: Review, file: File) {

    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['login']);
      return null;
    }

    return this.PatchReview(review).then(
      (res: number) => {
        if (res && res > -1) {
          if (file) {
            // upload the image
            return this.addReviewImage(file, res).then(
              (res: any) => {
                return res.result;
              },
              (error) => {
                this.handleObservableError(error);
              }
            )
          } else if (review.imgURL === null) {
            // delete the image
            return this.deleteReviewImage(res).then(
              (res: any) => {
                return res.result;
              },
              (error) => {
                this.handleObservableError(error);
              }
            );
          } else {
            // do nothing (just get reviews)
            return this.getReviews().then(
              (res: any) => {
                return res.result;
              },
              (error) => {
                this.handleObservableError(error);
              }
            )
          }
        }
      }
    )
  }

  private PatchReview(review: Review) {

    var affiliateLinksCSV = this.AffiliateLinksToCSV(review.affiliateLinks);

    return this.http.patch(environment.apiUrl + this.urlExtention + "update_review",
      {
        reviewID: review.id,
        name: review.name,
        oldPrice: review.oldPrice,
        newPrice: review.newPrice,
        imgURL: review.imgURL,
        date: review.date.toUTCString(),
        transcript: review.transcript,
        affiliatelinks: affiliateLinksCSV,
        filterTags: review.filterTags ? review.filterTags.join(',') : null,
        importance: review.importance
      },
      {
        headers: { 'Authorization': 'Bearer ' + this.authService.getToken() }
      }
    ).toPromise().then(
      (res: any) => {
        if (res.result === 'success') {
          return res.message;
        }
        return -1;
      },
      (error) => {
        this.handleObservableError(error);
        return -1;
      }
    )
  }

  private addReviewImage(file: File, reviewID: number) {

    const formData = new FormData();
    formData.append('image', file);
    formData.append('reviewID', "" + reviewID);

    return this.http.post(
      environment.apiUrl + this.urlExtention + 'add_image',
      formData,
      { headers: { 'Authorization': 'Bearer ' + this.authService.getToken() } }
    ).toPromise().then(
      (res: any) => {
        return res.result;
      },
      (error) => {
        this.handleObservableError(error);
        return null;
      }
    )
  }

  private deleteReviewImage(reviewID: number) {
    const formData = new FormData();
    formData.append('reviewID', "" + reviewID);

    return this.http.post(
      environment.apiUrl + this.urlExtention + 'delete_image',
      formData,
      { headers: { 'Authorization': 'Bearer ' + this.authService.getToken() } }
    ).toPromise().then(
      (res: any) => {
        return res.result;
      },
      (error) => {
        this.handleObservableError(error);
        return null;
      }
    )
  }

  public deleteReview(review: Review) {
    if (!this.authService.isLoggedIn()) {
      this.router.navigate(['login']);
      return null;
    }

    return this.http.request(
      'delete',
      environment.apiUrl + this.urlExtention + 'delete_review',
      {
        headers: { 'Authorization': 'Bearer ' + this.authService.getToken() },
        body: { reviewID: review.id }
      }
    ).toPromise().then(
      (res: any) => {
        return res.result;
      },
      (error) => {
        this.handleObservableError(error);
        return false;
      });
  }

  handleObservableError(e: any) {
    console.log(e);
  }

  private AffiliateLinksToCSV(affiliateLinks: string[][]) {
    let affiliateLinksCSV = "";

    if (!affiliateLinks) {
      affiliateLinks = [];
    }

    affiliateLinks.forEach(u => {
      affiliateLinksCSV += u.join(',') + ',';
    })

    affiliateLinksCSV = affiliateLinksCSV ? affiliateLinksCSV.substr(0, affiliateLinksCSV.length - 1) : "";
    return affiliateLinksCSV;
  }

}

import { Component, OnInit, ViewChild, ElementRef, Renderer2, AfterViewInit, Output, EventEmitter } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-toolbar',
  templateUrl: './toolbar.component.html',
  styleUrls: ['./toolbar.component.css']
})
export class ToolbarComponent implements OnInit, AfterViewInit {

  // TODO add images to: LOGO, SEARCH
  // TODO responsiveness
  // TODO hook up search to product reviews

  get wwwRoot(): string { return environment.wwwRoot; }
  get smallLogoURL(): string { return "assets/images/logo_small.svg" };
  get largeLogoURL(): string { return "assets/images/logo.svg" };
  get youtubeImgURL(): string { return "assets/images/youtube.svg" }

  @ViewChild('content')
  private contentEL: ElementRef;

  @ViewChild('searchContent')
  private searchContentEL: ElementRef;

  @ViewChild('searchInput')
  private searchInputEL: ElementRef;

  //@ViewChild('logo')
  //private logo: ElementRef;

  @Output('onSearchValueChange')
  private searchValueEmitter: EventEmitter<string> = new EventEmitter();

  inputValue: string = "";

  //@ViewChild('searchBox')
  //private searchBoxEL: ElementRef;

  //@ViewChild('searchBoxCover')
  //private searchBoxCoverEL: ElementRef;

  //@ViewChild('searchBoxInput')
  //private searchBoxInputEL: ElementRef;

  //@ViewChild('searchBoxCoverLabel')
  //private searchBoxCoverLabelEL: ElementRef;

  searchQuery: string = "";

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    //if (window.innerWidth  < 720) {
    //  this.logo.nativeElement.src = this.wwwRoot + this.smallLogoURL;
    //} else {
    //  this.logo.nativeElement.src = this.wwwRoot + this.largeLogoURL;
    //}
    //this.setSearchCoverLabelValue();
  }

  //TODO add iamges

  //openSearch() {
  //  this.renderer.removeClass(this.searchBoxEL.nativeElement, 'hidden');
  //  this.renderer.addClass(this.searchBoxCoverEL.nativeElement, 'hidden');

  //  this.searchBoxInputEL.nativeElement.focus();
  //  this.searchBoxInputEL.nativeElement.select();

  //  this.setSearchCoverLabelValue();
  //}

  //closeSearch() {
  //  this.renderer.addClass(this.searchBoxEL.nativeElement, 'hidden');
  //  this.renderer.removeClass(this.searchBoxCoverEL.nativeElement, 'hidden');

  //  this.setSearchCoverLabelValue();
  //}


  //private setSearchCoverLabelValue() {

  //  let val = this.searchQuery === '' ? 'Search ' : this.searchQuery;
  
  //  this.searchBoxCoverLabelEL.nativeElement.innerText = val;
  //}

  onWindowResize(event) {
    //if (event.target.innerWidth < 768) {
    //  this.logo.nativeElement.src = this.wwwRoot + this.smallLogoURL;
    //} else {
    //  this.logo.nativeElement.src = this.wwwRoot + this.largeLogoURL;
    //}
  }

  openSearch() {
    this.renderer.addClass(this.contentEL.nativeElement, 'hidden');
    this.renderer.removeClass(this.searchContentEL.nativeElement, 'hidden');
    this.searchInputEL.nativeElement.focus();
  }

  closeSearch() {
    this.renderer.removeClass(this.contentEL.nativeElement, 'hidden');
    this.renderer.addClass(this.searchContentEL.nativeElement, 'hidden');
    this.inputValue = "";
    this.onValueChange();
  }

  onValueChange() {
    this.searchValueEmitter.emit(this.inputValue);
  }

}

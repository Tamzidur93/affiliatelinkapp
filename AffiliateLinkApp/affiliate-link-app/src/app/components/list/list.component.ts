import { Component, OnInit, Input, Inject, AfterContentInit } from '@angular/core';
import { Review } from '../../classes/review';
import { MatDialog, MatDialogRef, MAT_DIALOG_DATA } from '@angular/material/dialog';
import { SimpleChanges, Output, Renderer2, ViewChild, ElementRef, EventEmitter } from '@angular/core';
import { FormControl } from '@angular/forms';
import { Filter } from 'src/app/classes/filter';
import { DateAdapter } from '@angular/material/core';
import { AddReviewDialog } from 'src/app/components/list/dialog_templates/add-dialog/add.review.dialog.cp';
import { EditReviewDialog } from 'src/app/components/list/dialog_templates/edit-review-dialog/edit.review.dialog.cp';
import { environment } from '../../../environments/environment';
import { MediaChange, ObservableMedia } from '@angular/flex-layout';
import { Observable } from 'rxjs';
import { trigger, state, style, transition, animate } from '@angular/animations';

@Component({
    selector: 'app-list',
    templateUrl: './list.component.html',
    styleUrls: ['./list.component.css'],
    animations: [
        trigger('fadeTranslateAnimation', [
            state('fadeOut', style({
                opacity: 0,
                transform: 'translateY(100px)'
            })),
            state('fadeIn', style({
                opacity: 1,
                transform: 'translateY(0%)'
            })),
            transition('* <=> *', animate('350ms ease-in-out')),
        ]),
    ]
})

export class ListComponent implements OnInit, AfterContentInit {

    @Input('reviews')
    reviews: Review[] = [];
    @Input('isLoading')
    isLoading = false;
    @Input('isAdminView')
    isAdminView: boolean = false;
    @Input('filters')
    filters: Filter[] = [];

    // constructor
    constructor(
        private dialog: MatDialog,
        private renderer: Renderer2,
        private observableMedia: ObservableMedia
    ) {
    }

    @ViewChild('reviewGrid')
    private reviewGrid: ElementRef;
    @ViewChild('content')
    private content: ElementRef;

    @Output('onAddReviewEvent')
    private addReviewEmitter: EventEmitter<Review> = new EventEmitter<Review>();

    @Output('onEditReviewEvent')
    private editReviewEmitter: EventEmitter<Review> = new EventEmitter<Review>();

    @Output('onDeleteReviewEvent')
    private deleteReviewEmitter: EventEmitter<any> = new EventEmitter<any>();

    @Output('onUploadReviewEvent')
    private uploadReviewEmitter: EventEmitter<any> = new EventEmitter<any>()

    private importanceDimensionMap = [{ cols: 1, rows: 1 }, { cols: 2, rows: 1 }, { cols: 2, rows: 2 }];
    gridByBreakpoint = {
        xl: 8,
        lg: 6,
        md: 4,
        sm: 2,
        xs: 1
    }
    breakPoint: number = this.gridByBreakpoint.lg;

    adminControlsEnabled: boolean = false;
    addButton: any; private

    state: string = 'fadeOut';

    private observer: IntersectionObserver;
    private initalLoadCount: number = 0;

    visibleClass = "visible";
    invisibleClass = "invisible";

    get wwwRoot(): string { return environment.wwwRoot; }
    get notFoundImgURL(): string { return "assets/images/not_found.svg" }

    ngOnInit() {
    }

    ngAfterContentInit(): void {
        this.observableMedia.asObservable().subscribe((change: MediaChange) => {
            this.breakPoint = this.gridByBreakpoint[change.mqAlias];
        })

        this.observer = new IntersectionObserver((ev, obs) => {
            if (this.initalLoadCount < 2) {

                if (this.initalLoadCount == 1) {
                    this.state = 'fadeIn';
                }

                this.initalLoadCount++;
            } else {
                this.observer.disconnect();
            }
        });
        this.observer.observe(this.content.nativeElement);
    }

    ngOnChanges(changes: SimpleChanges) {
    }

    // callback for list items
    private openReviewDetails(i: number) {

        // creates a dialog
        const dialogRef = this.dialog.open(
            ListDialogComponent,
            {
                panelClass: 'custom-dialog-container',
                data: this.reviews[i]
            }
        );
    }

    toggleAdminControls() {
        this.adminControlsEnabled = !this.adminControlsEnabled;
    }

    /* EVENT EMMITTERS */
    addReview() {
        // creates a dialog
        const dialogRef = this.dialog.open(
            AddReviewDialog,
            {
                panelClass: 'custom-dialog-container',
                data: {
                    filters: this.filters
                }
            }
        );

        dialogRef.afterClosed().subscribe(
            (dialogRes) => {
                if (dialogRes && dialogRes.res) {
                    let review = dialogRes.val;
                    this.addReviewEmitter.emit(review);
                }
            }
        )
    }

  editReview(i) {

    var savedReview = JSON.stringify(this.reviews[i]);
    // to object reference in memory
    var savedFile: File = this.reviews[i].file ? (this.reviews[i].file.slice() as File) : null;
    var fileName = this.reviews[i].file ? this.reviews[i].file.name : null;

    const dialogRef = this.dialog.open(
      EditReviewDialog,
      {
        panelClass: 'custom-dialog-container',
        data: {
          filters: this.filters,
          review: this.reviews[i]
        }
      }
    );

    dialogRef.afterClosed().subscribe(
      (dialogRes) => {
        if (dialogRes && dialogRes.res) {

          let review = dialogRes.val;

          if (savedReview !== JSON.stringify(this.reviews[i])) {
            this.reviews[i].dirty = true;
          }

          // this has to be the worst thing ever
          // force change detection
          var newFile: File = this.reviews[i].file ? (this.reviews[i].file.slice() as File) : null;
          var newFileName = this.reviews[i].file ? this.reviews[i].file.name : null;

          this.reviews[i] = JSON.parse(JSON.stringify(this.reviews[i]));
          this.reviews[i].date = new Date(Date.parse(this.reviews[i].date.toString()));
          // create a new file with same values as saved state
          this.reviews[i].file = newFile ? new File([newFile], newFileName) : null;

          this.editReviewEmitter.emit(review);
        } else {
          // reset changes when dialog is closed or cancelled
          //json.parse doesnt convert date strings to date objects 
          this.reviews[i] = JSON.parse(savedReview);
          var res = this.reviews[i].date.toString();
          this.reviews[i].date = new Date(Date.parse(res));
          // create a new file with same values as saved state
          this.reviews[i].file = savedFile ? new File([savedFile], fileName) : null;
        }
      }
    )
  }

    deleteReview(i) {

        const dialogRef = this.dialog.open(
            DeleteReviewDialog,
            {
                panelClass: 'custom-dialog-container',
                data: { review: this.reviews[i] }
            }
        );

        dialogRef.afterClosed().subscribe(
            (dialogRes) => {
                if (dialogRes && dialogRes.res) {
                    let review = dialogRes.val;
                    this.deleteReviewEmitter.emit(review);
                }
            }
        )
    }

    uploadReview(i) {
        const dialogRef = this.dialog.open(
            UploadReviewDialog,
            {
                panelClass: 'custom-dialog-container',
                data: { review: this.reviews[i] }
            }
        );

        dialogRef.afterClosed().subscribe(
            (dialogRes) => {
                if (dialogRes && dialogRes.res) {
                    let review = dialogRes.val;
                    this.uploadReviewEmitter.emit(review);
                }
            }
        )
    }

    getColsImport(i: number): number {

        if (this.breakPoint == 1) {
            return 1;
        }

        return this.importanceDimensionMap[this.reviews[i].importance].cols;
    }

    getRowsImport(i: number): number {
        if (this.breakPoint == 1) {
            return 1;
        }
        return this.importanceDimensionMap[this.reviews[i].importance].rows;
    }
}

@Component({
    styleUrls: ['./dialog_templates/delete.review.dialog.css'],
    templateUrl: './dialog_templates/delete.review.dialog.html'
})
export class DeleteReviewDialog {

    review: Review = new Review();
    constructor(
        public dialogRef: MatDialogRef<DeleteReviewDialog>,
        @Inject(MAT_DIALOG_DATA) private data: any) {
        this.review = data.review;
    }

    onOk() {
        this.dialogRef.close({ res: true, val: this.review });
    }

    onCancel() {
        this.dialogRef.close({ res: false, val: this.review });
    }
}

@Component({
    styleUrls: ['./dialog_templates/list.dialog.component.css'],
    templateUrl: './dialog_templates/list.dialog.component.html'
})
export class ListDialogComponent {


    get wwwRoot(): string { return environment.wwwRoot };

    constructor(
        public dialogRef: MatDialogRef<ListDialogComponent>,
        @Inject(MAT_DIALOG_DATA) public review: Review) {
    }

    // images
    imageReady(): boolean {
        return this.review.file && this.review.src;
    }

    // image isnt ready and review specifies imgURL
    imageProvided(): boolean {
        return !this.imageReady() && this.review.imgURL != null && this.review.imgURL != "";
    }

    imageExists(i): boolean {
        return this.review.src || (this.review.imgURL != null && this.review.imgURL != "");
    }
}

@Component({
    styleUrls: ['./dialog_templates/upload.review.dialog.css'],
    templateUrl: './dialog_templates/upload.review.dialog.html'
})
export class UploadReviewDialog {

    review: Review = new Review();

    constructor(
        public dialogRef: MatDialogRef<UploadReviewDialog>,
        @Inject(MAT_DIALOG_DATA) private data: any) {
        this.review = data.review;
    }

    onOk() {
        this.dialogRef.close({ res: true, val: this.review });
    }

    onCancel() {
        this.dialogRef.close({ res: false, val: this.review });
    }
}


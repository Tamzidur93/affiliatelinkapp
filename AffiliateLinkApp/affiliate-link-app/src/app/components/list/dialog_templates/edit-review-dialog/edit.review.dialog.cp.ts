import { MatDialogRef, MAT_DIALOG_DATA } from "@angular/material/dialog";
import { OnInit, Component, Inject, EventEmitter, ViewChild, ElementRef } from "@angular/core";
import { Filter } from "src/app/classes/filter";
import { DateAdapter } from "@angular/material/core";
import { Review } from "src/app/classes/review";
import { FormControl } from "@angular/forms";
// this is shit
import { environment } from '../../../../../environments/environment';

@Component({
  styleUrls: ['./edit.review.dialog.css'],
  templateUrl: './edit.review.dialog.html'
})
export class EditReviewDialog implements OnInit {

  filters: Filter[] = [];
  review: Review = new Review();
  backupReview: Review = new Review();

  ngOnInit(): void {
    //if (this.review.file) {

    //  this.review.src = null;

    //  console.log(this.review);

    //  let reader = new FileReader();

    //  reader.readAsDataURL(this.review.file);

    //  reader.onloadend = (ev: any) => {
    //    this.review.src = reader.result;
    //  };
    //}    
  }

  constructor(
    public dialogRef: MatDialogRef<EditReviewDialog>,
    @Inject(MAT_DIALOG_DATA) private data: any,
    private adapter: DateAdapter<any>
  ) {
    this.filters = data.filters;
    this.review = Object.assign(data.review);
    this.backupReview = Object.assign(this.backupReview, this.review);
  }

  accept: string = "images/*";
  onFileSelect: EventEmitter<File[]> = new EventEmitter();
  @ViewChild('inputFile') nativeInputFile: ElementRef;

  files: File[];
  //file: File;
  //src: any;
  filterTag: string;
  affiliateLink: string[] = new Array(2);

  get reviewTags(): string {
    if (this.review.filterTags) {
      return this.review.filterTags.join(',');
    } else {
      return 'none';
    }
  }
  get wwwRoot(): string { return environment.wwwRoot };

  onNativeInputFileSelect($event) {
    // change classes on file select
    this.files = $event.srcElement.files;

    if (!this.files || (this.files.length == 0)) {
      return;
    }

    console.log(this.files);

    //this.file = this.files[0];
    this.review.file = this.files[0];

    let reader = new FileReader();

    reader.readAsDataURL(this.review.file);

    reader.onloadend = (ev: any) => {
      this.review.src = reader.result;
    };
  }

  // images
  imageReady(): boolean {
    return this.review.file && this.review.src;
  }

  // image isnt ready and review specifies imgURL
  imageProvided(): boolean {
    return !this.imageReady() && this.review.imgURL != null && this.review.imgURL != "";
  }

  imageExists(): boolean {
    return this.review.src || (this.review.imgURL != null && this.review.imgURL != "");
  }

  selectFile() {
    this.nativeInputFile.nativeElement.click();
  }

  deselectFile() {
    this.files = null;
    //this.file = null;
    this.review.file = null;
    this.review.src = null;
    this.nativeInputFile.nativeElement.value = "";
  }

  removeExistingImage() {
    this.review.imgURL = null;
  }


  addAffiliateLink() {
    this.review.affiliateLinks.push(this.affiliateLink);
    this.affiliateLink = new Array(2);
  }

  deleteAffiliateLink(i) {
    console.log(i);
    if (i > -1) {
      this.review.affiliateLinks.splice(i, 1);
    }
  }

  // filters
  addFilter() {
    if (!this.review.filterTags) {
      this.review.filterTags = new Array<string>();
    }
    this.review.filterTags.push(this.filterTag);
    this.filterTag = "";
  }

  deleteFilter(value: string) {
    var index = this.review.filterTags.indexOf(value);
    if (index > -1) {
      this.review.filterTags.splice(index, 1);
    }
  }

  onSave() {
    console.log(this.review);
    this.dialogRef.close({ res: true, val: this.review });
  }

  onCancel() {
    console.log(this.review);
    Object.assign(this.review, this.backupReview);
    this.dialogRef.close({ res: false, val: this.review });
  }
}

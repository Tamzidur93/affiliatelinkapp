import { Component, OnInit, ViewChild, ElementRef, Input, Renderer2 } from '@angular/core';

@Component({
  selector: 'app-region-filter',
  templateUrl: './region-filter.component.html',
  styleUrls: ['./region-filter.component.css']
})
export class RegionFilterComponent implements OnInit {

  @Input('regions')
  private regions: string[] = ['Australia', 'United States'];
  @Input('selectedIndeces')
  private selectedIndeces: boolean[] = new Array<boolean>(this.regions.length);

  @ViewChild('regionContainer')
  private regionContainer: ElementRef;
  @ViewChild('label')
  private label: ElementRef;
  private regionsEL: any[] = []; // array of native elements
  //private regionRows: any[] = [];  // array of native elements -- dont need rows with flexbox layout

  // items per row needs to be based on region container width
  private itemsPerRow: number = 3;

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
  }

  ngAfterViewInit() {
    this.initRegionDisplay();
  }

  private initRegionDisplay() {
    const renderer = this.renderer;

    //let numberOfRows = this.regions.length / this.itemsPerRow + (this.regions.length % this.itemsPerRow ? 1 : 0);

    //this.regionRows.forEach(el => { renderer.removeChild(this.regionContainer, el) });

    //this.regionRows = [];
    this.regionsEL = [];

    //for (let i = 0; i < numberOfRows; i++) {

    //  let row = renderer.createElement('div');
    //  renderer.addClass(row, 'row');

    //  this.regionRows.push(row);
    //}

    for (let i = 0; i < this.regions.length; i++) {
      //let row = this.regionRows[Math.floor(i / this.itemsPerRow)];
      let region = renderer.createElement('div');

      region.innerText = this.regions[i];

      renderer.addClass(region, 'regionButton');

      if (this.selectedIndeces[i]) {
        renderer.addClass(region, 'regionSelectedButton');
      }
      
      renderer.setStyle(region, 'width', this.maxLength() + 'ch')
      renderer.setStyle(region, 'max-width', '40ch');

      renderer.listen(region, 'click', () => this.onClick(i));

      renderer.appendChild(this.regionContainer.nativeElement, region);
      //renderer.appendChild(this.regionContainer.nativeElement, row);

      this.regionsEL.push(region);
    }
  }

  private maxLength(): number {
    let res = -1;
    this.regions.forEach(cat => { cat.length > res ? res = cat.length : res; });
    return res;
  }

  private onClick(i) {

    // update selected index map
    this.selectedIndeces[i] = !this.selectedIndeces[i];

    // set classes
    if (this.selectedIndeces[i]) {
      this.renderer.addClass(this.regionsEL[i], 'regionSelectedButton');
    } else {
      this.renderer.removeClass(this.regionsEL[i], 'regionSelectedButton');
    }

    // set label value
    let label = '';
    let res = false; // assume no elements are selected

    for (let i = 0; i < this.selectedIndeces.length; i++) {

      res = res || this.selectedIndeces[i]; // if any are selected res = true

      if (this.selectedIndeces[i]) {
        label += this.regions[i] + ', ';
      }
    }   

    if (res) { // if there is a selected value then label will have ending comma
      label = label.substring(0, label.length - 2);
    }else { // if there is no selected value then label will not be set (set it to default here)
      label = 'All Regions'
    }

    this.label.nativeElement.innerText = label;

    
  }
}

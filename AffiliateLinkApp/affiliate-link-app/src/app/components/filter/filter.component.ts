import { Component, OnInit, Input, ViewChildren, Renderer2 } from '@angular/core';
import { AfterViewInit } from '@angular/core';
import { DoCheck, AfterContentInit, AfterContentChecked, ElementRef, ViewChild, QueryList, EventEmitter, Output, Inject } from '@angular/core';
import { Filter } from 'src/app/classes/filter';
import { MatDialog, MAT_DIALOG_DATA, MatDialogRef } from '@angular/material/dialog';
import { FilterService } from 'src/app/services/filter.service';
import { trigger, state, style, transition, animate } from '@angular/animations/';
import { Element } from '@angular/compiler';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-filter',
  templateUrl: './filter.component.html',
  styleUrls: ['./filter.component.css'],

  animations: [
    trigger('fadeTranslateAnimation', [
      state('fadeOut', style({
        opacity: 0,
        transform: 'translateY(100px)'
      })),
      state('fadeIn', style({
        opacity: 1,
        transform: 'translateY(0%)'
      })),
      transition('* <=> *', animate('350ms ease-in-out')),
    ]),
  ]
})
export class FilterComponent implements OnInit, AfterViewInit {

  @Input('categories') categories: Filter[] = [];
  @Input('selectedIndeces') private selectedIndeces: boolean[] = new Array<boolean>(this.categories.length);
  @Input('isAdminView') isAdminView: boolean = false;
  @Output('onCategoryToggled') private onSelectEmitter: EventEmitter<number> = new EventEmitter();

  @Output('onAddFilterEvent')
  private addFilterEmitter: EventEmitter<Filter> = new EventEmitter<Filter>();
  @Output('onEditFilterEvent')
  private editFilterEmitter: EventEmitter<Filter> = new EventEmitter<Filter>();
  @Output('onDeleteFilterEvent')
  private deleteFilterEmitter: EventEmitter<Filter> = new EventEmitter<Filter>();

  @ViewChild('categoryContainer') categoryContainer: ElementRef;
  @ViewChild('categoryTitle') categoryTitle: ElementRef;
  @ViewChildren('categoryEL') categoryELs: QueryList<ElementRef>;

  private itemsPerRow: number = 4;
  private addButton: any;

  adminControlsEnabled: boolean = false;

  // number of times inital 'load' animation has been performed
  loadAnimationCount: number = 0;
  // intersectionObserver for initial scroll down animation
  observer: IntersectionObserver = null;
  state: string = 'fadeOut';

  get wwwRoot(): string { return environment.wwwRoot };
  get noCategoryFoundImgURL(): string { return "assets/images/not_found.svg" };

  constructor(private renderer: Renderer2, private dialog: MatDialog, private filterService: FilterService) { }

  ngOnInit() {
    // initialize intersectionObserver
  }

  ngAfterViewInit() {
    this.initCategoryDisplay();

    this.observer = new IntersectionObserver((entry, observer) => {
      if (this.loadAnimationCount < 2) {

        if (this.loadAnimationCount === 1) {
          this.state = 'fadeIn';
        }

        this.loadAnimationCount++;
      } else {
        this.observer.unobserve(this.categoryTitle.nativeElement);
      }
    });

    this.observer.observe(this.categoryTitle.nativeElement);
  }

  private initCategoryDisplay() {
    const renderer = this.renderer;
    this.categoryELs.forEach((el: ElementRef) => {
      renderer.setStyle(el.nativeElement, 'width', this.maxLength() + 'ch');
    });
  }

  private maxLength(): number {
    let res = -1;
    this.categories.forEach(cat => { cat.name.length > res ? res = cat.name.length : res; });
    return res;
  }

  private onClick(i) {
    // update selected index map    
    this.onSelectEmitter.emit(i);

    if (this.selectedIndeces[i]) {
      this.renderer.addClass(this.categoryELs.toArray()[i].nativeElement, 'categorySelectedButton');
    } else {
      this.renderer.removeClass(this.categoryELs.toArray()[i].nativeElement, 'categorySelectedButton');
    }
  }

  // TODO add and remove hover effects
  private toggleAdminControls() {
    this.adminControlsEnabled = !this.adminControlsEnabled;

    //if (this.adminControlsEnabled) {
    //  //disable button hovers

    //  this.addButton = this.renderer.createElement('div');
    //  this.renderer.addClass(this.addButton, 'addButton');
    //  this.renderer.appendChild(this.categoryContainer.nativeElement, this.addButton);

    //  var catButtonContainer = this.renderer.createElement('div');
    //  this.renderer.addClass(catButtonContainer, 'catButtonContainer');

    //  catButtonContainer.innerHTML = "+";

    //  this.renderer.appendChild(this.addButton, catButtonContainer);

    //  this.renderer.listen(this.addButton, 'click', (evt) => {
    //    this.addFilter();
    //  });

    //  // add 'add button' to list
    //} else {
    //  // remove button from list
    //  this.renderer.removeChild(this.categoryContainer.nativeElement, this.addButton);
    //}
  }

  private addFilter() {
    // creates a dialog
    const dialogRef = this.dialog.open(
      FilterAddDialog,
      {
        panelClass: 'custom-dialog-container',
      }
    );

    dialogRef.afterClosed().subscribe(
      (dialogRes: any) => {
        if (dialogRes && dialogRes.res) {
          var filter = dialogRes.val;
          this.addFilterEmitter.emit(filter);
        }
      });

    // update backend
    //this.filterService.addFilter(res.val).then(
    //  (res: any) => {
    //    this.categories = res
    //  }
    //);
  }

  private editFilter(i) {
    // creates a dialog
    const dialogRef = this.dialog.open(
      FilterEditDialog,
      {
        panelClass: 'custom-dialog-container',
        data: this.categories[i]
      }
    );


    //TODO add loading cycle to prevent multiple updates
    // call back for after dialog is closed
    dialogRef.afterClosed().subscribe(
      dialogRes => {
        if (dialogRes && dialogRes.res) {
          // update backend
          var filter = dialogRes.val;
          this.editFilterEmitter.emit(filter);
          //this.filterService.updateFilter(this.categories[i]).then(
          //  (res: Filter[]) => { this.categories = res }
          //);
        }
      });
  }

  private deleteFilter(i) {
    const dialogRef = this.dialog.open(
      FilterDeleteDialog,
      {
        panelClass: 'custom-dialog-container',
        data: this.categories[i]
      }
    );

    dialogRef.afterClosed().subscribe(
      dialogRes => {
        if (dialogRes && dialogRes.res) {
          var filter = dialogRes.val;
          this.deleteFilterEmitter.emit(filter);
          // update backend
          //this.filterService.deleteFilter(this.categories[i]).then(
          //  (res: Filter[]) => { this.categories = res }
          //);
        }
      });
  }

}

// TODO add validation, add character limit
@Component({
  styleUrls: ['filter.add.dialog.css'],
  templateUrl: 'filter.add.dialog.html'
})
export class FilterAddDialog {

  constructor(public dialogRef: MatDialogRef<FilterAddDialog>) {
  }

  filter: Filter = new Filter();

  onSave() {
    this.dialogRef.close({ res: true, val: this.filter });
  }

  onCancel() {
    // close dialog with no event
    this.dialogRef.close({ res: false, val: this.filter });
  }
}

// TODO add validation, add character limit
@Component({
  styleUrls: ['filter.edit.dialog.css'],
  templateUrl: 'filter.edit.dialog.html'
})
export class FilterEditDialog {

  constructor(
    public dialogRef: MatDialogRef<FilterEditDialog>,
    @Inject(MAT_DIALOG_DATA) public filter: Filter) {
  }

  newName = this.filter.name;

  onSave() {
    // make filters name new name
    this.filter.name = this.newName
    // close dialog with yes event
    this.dialogRef.close({ res: true, val: this.filter });
  }

  onCancel() {
    // close dialog with no event
    this.dialogRef.close({ res: false, val: this.filter });
  }
}

@Component({
  styleUrls: ['filter.delete.dialog.css'],
  templateUrl: 'filter.delete.dialog.html'
})
export class FilterDeleteDialog {

  constructor(
    public dialogRef: MatDialogRef<FilterDeleteDialog>,
    @Inject(MAT_DIALOG_DATA) public filter: Filter) {
  }

  onOk() {
    // close dialog with true event
    this.dialogRef.close({ res: true, val: this.filter });
  }

  onCancel() {
    // close dialog with false event
    this.dialogRef.close({ res: false, val: this.filter });
  }
}

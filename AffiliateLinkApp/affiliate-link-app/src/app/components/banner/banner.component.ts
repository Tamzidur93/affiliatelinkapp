import { Component, OnInit, EventEmitter, Output, ElementRef, ViewChild, Renderer2 } from '@angular/core';
import { environment } from '../../../environments/environment';

@Component({
  selector: 'app-banner',
  templateUrl: './banner.component.html',
  styleUrls: ['./banner.component.css']
})
export class BannerComponent implements OnInit {

  @Output('onScrollButtonClicked') clickEmitter: EventEmitter<any> = new EventEmitter();

  @ViewChild('background')
  private background : ElementRef;

  get wwwRoot(): string { return environment.wwwRoot };
  get backgroundImageURL(): string { return 'assets/images/background.jpg' }

  constructor(private renderer: Renderer2) { }

  ngOnInit() {
    var bgImg = new Image();
    bgImg.src = this.wwwRoot + this.backgroundImageURL;

    bgImg.onload = (event) => {
      this.renderer.removeClass(this.background.nativeElement, 'fadeOut');
      this.renderer.addClass(this.background.nativeElement, 'fadeIn');
    }

    //console.log(this.wwwRoot + this.backgroundImageURL);

    this.renderer.setStyle(this.background.nativeElement, 'background', `url(${this.wwwRoot + this.backgroundImageURL})`);
    this.renderer.setStyle(this.background.nativeElement, 'background-repeat', `no-repeat)`);
    this.renderer.setStyle(this.background.nativeElement, 'background-position', `center center`);
    this.renderer.setStyle(this.background.nativeElement, 'background-size', `cover`);
    this.renderer.setStyle(this.background.nativeElement, 'background-attachment', `fixed`);
    this.renderer.setStyle(this.background.nativeElement, 'background-color', `black`);
    //this.renderer.setStyle(this.background.nativeElement, 'background-color', `black`);

    //background - repeat: no - repeat;
    //background - position: center center;
    //background - size: cover;
    //background - attachment: fixed;* /


  }

  scrollDown() {
    this.clickEmitter.emit();
  }


}

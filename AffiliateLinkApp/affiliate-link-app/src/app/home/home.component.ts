import { Component, OnInit } from '@angular/core';
import { ElementRef } from '@angular/core';
import { ViewChild, Renderer2 } from '@angular/core';
import { Review } from 'src/app/classes/review';
import { FilterService } from 'src/app/services/filter.service';
import { Filter } from 'src/app/classes/filter';
import { ReviewService } from 'src/app/services/review.service';
import { style, state, trigger, animate, transition, AnimationEvent } from '@angular/animations';
import { environment } from '../../environments/environment';

@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],

  animations: [
    trigger('fadeOutAnimation', [
      state('fadeOut', style({
        opacity: 0
      })),
      state('fadeIn', style({
        opacity: 1
      })),
      transition('* <=> *', animate('100ms ease-in')),
    ]),
  ]
})
export class HomeComponent implements OnInit {

  // components
  @ViewChild('appToolbar', { read: ElementRef })
  private appToolbar: ElementRef;
  @ViewChild('appBanner', { read: ElementRef })
  private appBanner: ElementRef;
  @ViewChild('bodyContent', { read: ElementRef })
  private bodyContent: ElementRef;
  @ViewChild('appFilter', { read: ElementRef })
  private appFilter: ElementRef;
  @ViewChild('appList', { read: ElementRef })
  private appList: ElementRef;
  @ViewChild('appFooter', { read: ElementRef })
  private appFooter: ElementRef;
  @ViewChild('footerContent')
  private footerContent: ElementRef;

  // loading component
  @ViewChild('content')
  private content: ElementRef;
  @ViewChild('loadingContent')
  private loadingContent: ElementRef;

  // animation states for main content loading
  loaderState: string = 'fadeIn';
  contentState: string = 'fadeOut';

  // animation states for search animation
  searchState: string = "fadeTranslateUp";

  // data for filter component
  categories: Filter[] = [];
  // used for filter reviews 
  selectedIndeces: boolean[] = [];
  // reviews that are provided to list component
  filteredReviews: Review[] = [];
  // reviews recieved from backend services
  private reviews: Review[] = [];

  constructor(
    private renderer: Renderer2,
    private filterService: FilterService,
    private reviewService: ReviewService
  ) { }


  // load up data before components are displayed
  ngOnInit() {
    this.filterService.getFilters().then(
      // sequential loading
      // TODO make this concurrent
      (res: Filter[]) => {
        // initalize filters
        this.initializeFilters(res);
        // initialize reviews
        this.reviewService.getReviews().then(
          (res: Review[]) => {
            this.initializeReviews(res);
            // call function for load animation
            this.loadCompleted();
          }
        )
      }
    );
  }

  /* DATA HANDLING */
  // initalize reviews to either empty or provided reviews
  initializeReviews(res: Review[]) {
    if (res) {
      this.reviews = res;
    } else {
      this.reviews = []
    }
    // filter review based on enabled filters
    this.filterReviewContent();
  }

  // initialize filtesr to either empty or provided filters
  initializeFilters(res: Filter[]) {
    if (res) {
      this.categories = res;
    } else {
      // TODO CHANGE THIS FOR PRODUCTION
      this.categories = []
    }

    // initalize filter-selection map
    this.selectedIndeces = new Array<boolean>(this.categories.length);
  }

  /* ANIMATION */
  // switch content states to start fadeIn/fadeOut animations
  loadCompleted() {
    this.renderer.removeClass(this.content.nativeElement, 'hidden');

    this.loaderState = 'fadeOut';
    this.contentState = 'fadeIn';
  }

  // used to hide loading spinner once its fade animation is completed
  loadingAnimationCompleted(e: AnimationEvent) {
    if (e.fromState === 'fadeIn' && e.toState === 'fadeOut') {
      this.renderer.addClass(this.loadingContent.nativeElement, 'hidden');
    }
  }

  /* EVENT HANDLERS */
  // scroll content down bodycontent when scrollButton is clicked
  scrollDown() {
    // offset pixels for toolbar height
    var toolbarOffset = 64;
    var elementPosition = this.bodyContent.nativeElement.getBoundingClientRect().top;
    var scrollTop = (window.pageYOffset !== undefined) ? window.pageYOffset : ((document.documentElement || document.body.parentNode || document.body) as any).scrollTop;
    var offsetPosition = elementPosition - toolbarOffset + scrollTop;

    window.scrollTo({
      top: offsetPosition,
      behavior: "smooth"
    })
  }

  // toggles a categories' mapped toggleable value when a category filter is clicked
  onCategoryToggled(i: number) {
    this.selectedIndeces[i] = !this.selectedIndeces[i];
    // filter list content
    this.filterReviewContent();
  }

  // hides banner, filter and footer components when any value is changed in search component
  onSearchEvent(e) {
    if (e != "") {
      // hide filters, banner footer?
      this.renderer.addClass(this.appBanner.nativeElement, 'hidden');
      this.renderer.addClass(this.appFilter.nativeElement, 'hidden');
      this.renderer.addClass(this.appFooter.nativeElement, 'hidden');
    } else {
      // show filters, banner, footer
      this.renderer.removeClass(this.appBanner.nativeElement, 'hidden');
      this.renderer.removeClass(this.appFilter.nativeElement, 'hidden');
      this.renderer.removeClass(this.appFooter.nativeElement, 'hidden');
    }

    // filter through reviews using keywords
    this.filterReviewContentKeyword(e);
  }

  /* DATA TRANSFORMERS */
  // filter through backend data using selected categories' values
  filterReviewContent() {
    this.filteredReviews = new Array<Review>();

    // dont perform filtration if no values are selected
    let selectedIndecesVal = false;
    this.selectedIndeces.forEach(v => { v ? selectedIndecesVal = true : ''; })
    if (!selectedIndecesVal) {
      this.filteredReviews = this.reviews;
      return;
    }

    for (let i: number = 0; i < this.selectedIndeces.length; i++) {
      if (this.selectedIndeces[i]) {

        selectedIndecesVal = true;
        // filter out based on selected categories
        var res = this.reviews.filter(r => { return r.filterTags.includes(this.categories[i].name) });

        res.forEach(r => {
          if (!this.filteredReviews.includes(r)) {
            // add reviews to final filtered list if there are none which are the same
            this.filteredReviews.push(r)
          }
        });
      }
    }
  }

  // filter through backend data using provided keywords
  filterReviewContentKeyword(keyword: string) {

    // dont perform filteration if no keyword provided
    if (keyword === '') {
      this.filteredReviews = this.reviews;
      return;
    }

    const reviews = this.reviews;

    this.filteredReviews = new Array<Review>();

    for (let i = 0; i < reviews.length; i++) {
      let review = reviews[i];
      for (let j = 0; j < review.filterTags.length; j++) {
        let filterTag = review.filterTags[j];

        // names and filter tags
        var condition = filterTag.toLowerCase().includes(keyword.toLowerCase().trim());
        condition = condition || review.name.toLowerCase().includes(keyword.toLowerCase().trim());

        if (condition) {

          if (!this.filteredReviews.includes(reviews[i])) {
            // add reviews to final filtered list if there are none which are the same
            this.filteredReviews.push(reviews[i]);
          }
          break;
        };
      }
    }
  }


}

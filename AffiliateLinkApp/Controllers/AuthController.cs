﻿using AffiliateLinkApp.Database;
using AffiliateLinkApp.Entities;
using AffiliateLinkApp.Jwt;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Extensions.Options;
using Newtonsoft.Json;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AffiliateLinkApp.Controllers
{
    [Route("api/[controller]/")]
    public class AuthController : Controller
    {
        private readonly UserManager<AppUser> _userManager;
        private readonly IJwtFactory _jwtFactory;
        private readonly AppDbContext _appDbContext;
        private readonly JwtIssuerOptions _jwtOptions;

        public AuthController(UserManager<AppUser> userManager, IJwtFactory jwtFactory, AppDbContext appDbContext, IOptions<JwtIssuerOptions> options)
        {
            _userManager = userManager;
            _jwtFactory = jwtFactory;
            _appDbContext = appDbContext;
            _jwtOptions = options.Value;
        }

        [HttpGet("is_authenticated")]
        public ActionResult IsAuthenticated()
        {
            return new JsonResult(HttpContext.User.Identity.IsAuthenticated);
        }

        [HttpPost("login")]
        [AllowAnonymous]
        public async Task<ActionResult> Login([FromBody] CredentialsDTO dto)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var claimIdentities = await GetClaimsIdentity(dto.Email, dto.Password);

            if (claimIdentities == null)
            {
                return new JsonResult(new { result = "failure", message = "Invalid username or password" });
            }

            var jwt = await JwtTokens.GenerateJwt(claimIdentities, _jwtFactory, dto.Email, _jwtOptions, new JsonSerializerSettings { Formatting = Formatting.Indented });

            return new JsonResult(new { result = "success", message = jwt });
        }

        private async Task<ClaimsIdentity> GetClaimsIdentity(string email, string password)
        {
            if (string.IsNullOrEmpty(email) || string.IsNullOrEmpty(password))
            {
                return null;
            }

            var user = await _userManager.FindByNameAsync(email);

            if (user != null)
            {
                if (await _userManager.CheckPasswordAsync(user, password))
                {
                    return await Task.FromResult(_jwtFactory.GenerateClaimsIdentity(email, user.Id, JwtAppConstants.Strings.JwtClaims.Admin));
                }
            }

            return null;
        }

        public class CredentialsDTO
        {
            [Required]
            public string Email { get; set; }
            [Required]
            public string Password { get; set; }
        }

    }
}

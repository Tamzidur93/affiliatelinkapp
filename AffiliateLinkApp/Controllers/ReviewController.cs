﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Http.Headers;
using System.Threading.Tasks;
using AffiliateLinkApp.Database;
using AffiliateLinkApp.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AffiliateLinkApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]/")]
    public class ReviewController : Controller
    {
        private AppDbContext _dbContext;
        private IHostingEnvironment _environment;

        public ReviewController(AppDbContext dbContext, IHostingEnvironment environment)
        {
            _dbContext = dbContext;
            _environment = environment;
        }

        [HttpGet("get_reviews")]
        public ActionResult GetReviews()
        {
            return Json(new { result = "success", message = _dbContext.Reviews });
        }

        [Authorize]
        [HttpPost("add_review")]
        public async Task<ActionResult> AddReview([FromBody] Review review)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _dbContext.Reviews.Add(review);

            try
            {
                var res = await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new JsonResult(new { result = "failure", message = $"Error when saving review: " + e.Message });
            }

            return Json(new { result = "success", message = review.ReviewID });
        }

        [Authorize]
        [HttpPost("delete_image")]
        public async Task<ActionResult> DeleteImage([FromForm] int reviewID)
        {
            var review = _dbContext.Reviews.Where(r => r.ReviewID == reviewID).FirstOrDefault();

            if (review == null)
            {
                return new JsonResult(new { result = "failure", message = $"No review with id: {reviewID} found" });
            }

            var imgURL = review.ImgURL;

            review.ImgURL = null;

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                return Json(new { result = "failure", message = $"Couldnt remove image for review - id: {reviewID}" });
            }

            if (imgURL != null)
            {
                var delFilePath = Path.Combine(_environment.ContentRootPath, imgURL);
                if (System.IO.File.Exists(delFilePath))
                {
                    System.IO.File.Delete(delFilePath);
                }
            }

            return Json(new { result = "success", message = _dbContext.Reviews });
        }

        [Authorize]
        [HttpPost("add_image")]
        public async Task<ActionResult> AddImage([FromForm] int reviewID)
        {
            var res = "";

            if (reviewID < 0)
            {
                return Json(new { result = "failure", message = $"Expected positive reviewID value, got: {reviewID}" });
            }

            if (HttpContext.Request.Form.Files == null || HttpContext.Request.Form.Files.Count == 0)
            {
                return Json(new { result = "failure", message = $"No files found" });
            }

            var files = HttpContext.Request.Form.Files;

            var review = _dbContext.Reviews.Where(r => r.ReviewID == reviewID).FirstOrDefault();

            foreach (var file in files)
            {

                if (review == null)
                {
                    return new JsonResult(new { result = "failure", message = $"No review with id: {reviewID} found" });
                }

                // delete stored image
                if (review.ImgURL != null)
                {
                    var delFilePath = Path.Combine(_environment.ContentRootPath, review.ImgURL);
                    if (System.IO.File.Exists(delFilePath))
                    {
                        System.IO.File.Delete(delFilePath);
                    }
                    review.ImgURL = null;
                }

                var fileName = ContentDispositionHeaderValue.Parse(file.ContentDisposition).FileName.Trim('"');
                var myUniqueFileName = string.Format(@"{0}", DateTime.Now.Ticks);
                myUniqueFileName += Path.GetExtension(fileName);
                fileName = Path.Combine(_environment.ContentRootPath, "assets/images/") + $@"{myUniqueFileName}";
                res = "assets/images/" + myUniqueFileName;
                using (FileStream fs = System.IO.File.Create(fileName))
                {
                    await file.CopyToAsync(fs);
                    fs.Flush();
                }

                review.ImgURL = res;

                try
                {
                    await _dbContext.SaveChangesAsync();
                }
                catch (Exception e)
                {
                    return Json(new { result = "failure", message = $"Couldnt save image for review - id: {reviewID}" });
                }                
            }
            return Json(new { result = "success", message = _dbContext.Reviews });
        }

        [Authorize]
        [HttpDelete("delete_review")]
        public async Task<ActionResult> DeleteReview([FromBody] Review review)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var res = _dbContext.Reviews.Where(r => r.ReviewID == review.ReviewID).FirstOrDefault();

            if (res == null)
            {
                return new JsonResult(new { result = "failure", message = $"No review with id {review.ReviewID} found." });
            }

            if (res.ImgURL != null)
            {
                var delFilePath = Path.Combine(_environment.ContentRootPath, res.ImgURL);
                if (System.IO.File.Exists(delFilePath))
                {
                    System.IO.File.Delete(delFilePath);
                }
                res.ImgURL = null;
                // delete stored image
            }

            _dbContext.Reviews.Remove(res);

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new JsonResult(new { result = "failure", message = $"Error when saving review: " + e.Message });
            }

            return Json(new { result = "success", message = "" });
        }

        [Authorize]
        [HttpPatch("update_review")]
        public async Task<ActionResult> UpdateReview([FromBody] Review review)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var res = _dbContext.Reviews.Where(r => r.ReviewID == review.ReviewID).FirstOrDefault();

            if (res == null)
            {
                // add review
                res = new Review
                {
                    AffiliateLinks = review.AffiliateLinks,
                    Date = review.Date,
                    FilterTags = review.FilterTags,
                    //ImgURL = review.ImgURL,
                    Name = review.Name,
                    NewPrice = review.NewPrice,
                    OldPrice = review.OldPrice,
                    ReviewID = review.ReviewID,
                    Transcript = review.Transcript,
                    Importance = review.Importance
                };

                _dbContext.Reviews.Add(res);
            }
            else
            {
                // update review
                res.AffiliateLinks = review.AffiliateLinks;
                res.Date = review.Date;
                res.FilterTags = review.FilterTags;
                //res.ImgURL = review.ImgURL;
                res.Name = review.Name;
                res.NewPrice = review.NewPrice;
                res.OldPrice = review.OldPrice;
                res.ReviewID = review.ReviewID;
                res.Transcript = review.Transcript;
                res.Importance = review.Importance;
            }

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new JsonResult(new { result = "failure", message = $"Error when saving review: " + e.Message });
            }

            return Json(new { result = "success", message = res.ReviewID });
        }
    }
}
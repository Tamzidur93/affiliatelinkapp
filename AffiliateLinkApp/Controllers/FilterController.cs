﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;
using AffiliateLinkApp.Database;
using AffiliateLinkApp.Entities;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;

namespace AffiliateLinkApp.Controllers
{
    [Produces("application/json")]
    [Route("api/[controller]")]
    public class FilterController : Controller
    {
        private AppDbContext _dbContext;

        public FilterController(AppDbContext context)
        {
            _dbContext = context;
        }

        [HttpGet("get_filters")]
        public ActionResult GetFilters()
        {
            return Json(new { result = "success", message = _dbContext.Filters });
        }

        [Authorize]
        [HttpPost("add_filter")]
        public async Task<ActionResult> AddFilter([FromBody] Filter filter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            _dbContext.Add(filter);

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new JsonResult(new { result = "failure", message = $"Error when saving filter: " + e.Message });
            }

            return Json(new { result = "success", message = _dbContext.Filters });
        }

        [Authorize]
        [HttpPatch("update_filter")]
        public async Task<ActionResult> UpdateFilter([FromBody] Filter filter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            Helpers.Helpers.PrintRequestBody(this.Request.Body);


            var res = _dbContext.Filters.Where(f => f.FilterID == filter.FilterID).FirstOrDefault();

            if (res == null)
            {
                // add filter
                _dbContext.Add( new Filter { Name = filter.Name});
            }
            else
            {
                res.Name = filter.Name;
            }

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new JsonResult(new { result = "failure", message = $"Error when saving filter: " + e.Message });
            }

            return Json(new { result = "success", message = _dbContext.Filters });
        }

        [Authorize]
        [HttpDelete("delete_filter")]
        public async Task<ActionResult> DeleteFilter([FromBody] Filter filter)
        {
            if (!ModelState.IsValid)
            {
                return BadRequest(ModelState);
            }

            var res = _dbContext.Filters.Where(f => f.FilterID == filter.FilterID).FirstOrDefault();

            if (res == null)
            {
                return new JsonResult(new { result = "failure", message = $"No filter with id {filter.FilterID} found." });
            }

            _dbContext.Remove(res);

            try
            {
                await _dbContext.SaveChangesAsync();
            }
            catch (Exception e)
            {
                Console.WriteLine(e);
                return new JsonResult(new { result = "failure", message = $"Error when saving filter: " + e.Message });
            }

            return Json(new { result = "success", message = _dbContext.Filters });
        }
    }
}

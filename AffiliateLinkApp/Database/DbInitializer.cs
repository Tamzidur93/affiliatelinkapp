﻿using AffiliateLinkApp.Entities;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AffiliateLinkApp.Database
{
    public class DbInitializer
    {
        public async static void Seed(AppDbContext context, UserManager<AppUser> userManager, IConfigurationRoot configuration)
        {
            //context.Database.EnsureCreated();
            context.Database.Migrate();

            // USERNAME MUST BE AN EMAIL ADDRESS //
            var username = configuration.GetSection("AdminCredentials")["Username"];
            var password = configuration.GetSection("AdminCredentials")["Password"];

            var res = context.Users.Where(u => u.Email == username).FirstOrDefault();

            //var users = userManager.Users;

            // ensure only 1 user exists in the database (admin)
            //foreach (var user in users)
            //{
            //    await userManager.DeleteAsync(user);
            //}

            if (res == null)
            {
                await userManager.CreateAsync(new AppUser
                {
                    Email = username,
                    UserName = username,
                }, password);
            }
        }
    }
}

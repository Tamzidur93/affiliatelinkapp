﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Threading.Tasks;

namespace AffiliateLinkApp.Helpers
{
    public static class Helpers
    {
        public static void PrintRequestBody(Stream body)
        {
            MemoryStream memstream = new MemoryStream();
            body.Position = 0;
            body.CopyTo(memstream);
            memstream.Position = 0;
            using (StreamReader reader = new StreamReader(memstream))
            {
                string text = reader.ReadToEnd();
                Console.WriteLine(text);
            }
        }
    }
}

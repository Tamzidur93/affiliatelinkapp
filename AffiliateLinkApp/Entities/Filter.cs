﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AffiliateLinkApp.Entities
{
    public class Filter
    {
        [Key]
        public int FilterID { get; set; }
        public string Name { get; set; }
    }
}

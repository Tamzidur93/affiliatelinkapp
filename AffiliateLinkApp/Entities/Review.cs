﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace AffiliateLinkApp.Entities
{
    public class Review
    {
        [Key]
        public int ReviewID { get; set; }
        public string ImgURL { get; set; }
        public string Name { get; set; }
        public string OldPrice { get; set; }
        public string NewPrice { get; set; }
        public DateTime Date { get; set; }
        public string Transcript { get; set; }
        // entity framework doesnt allow primitive[]
        public string AffiliateLinks { get; set; } // string[][] - map of region(US, AUS) to affliateLink(...)
        public string FilterTags { get; set; } // string[] 
        // determines image col and row span
        // range 0-2
        // 0 = 1 x 1
        // 1 = 2 x 1
        // 2 = 2 x 2
        public string Importance { get; set; }
    }
}


//imgUrl: string;
//  name: string;
//  oldPrice: number;
//  newPrice: number;
//  date: string;
//  transcript: string;
//  affiliateLinks: string[][];
//  regionTags: string[];
//  filterTags: string[];

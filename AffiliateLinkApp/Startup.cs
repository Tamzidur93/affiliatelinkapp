﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using AffiliateLinkApp.Database;
using AffiliateLinkApp.Entities;
using AffiliateLinkApp.Helpers;
using AffiliateLinkApp.Jwt;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.DependencyInjection.Extensions;
using Microsoft.Extensions.FileProviders;
using Microsoft.IdentityModel.Tokens;

namespace AffiliateLinkApp
{
    public class Startup
    {
        private readonly IConfiguration _configuration;
        private readonly IHostingEnvironment _env;
        private SymmetricSecurityKey _signingKey = null;
        private string _secret = "";
        private bool _production = false;

        public Startup(IConfiguration configuration, IHostingEnvironment env)
        {
#if DEBUG
            _configuration = configuration;
#else
            var builder = new ConfigurationBuilder()
                .SetBasePath(env.ContentRootPath)
                .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true);

            _configuration = builder.Build();
#endif
            _env = env;
        }

        // This method gets called by the runtime. Use this method to add services to the container.
        // For more information on how to configure your application, visit https://go.microsoft.com/fwlink/?LinkID=398940
        public void ConfigureServices(IServiceCollection services)
        {

            _secret = _configuration.GetSection("JWTKeySettings")["Secret"];
            _production = Boolean.Parse(_configuration.GetSection("Configuration")["Production"]);
            _signingKey = new SymmetricSecurityKey(Encoding.ASCII.GetBytes(_secret));

            services.AddMvc();
            if (!_production)
            {
                services.AddDbContext<AppDbContext>(options => options.UseSqlServer(_configuration.GetConnectionString("SqlConnectionString")));
            }
            else
            {
                services.AddDbContext<AppDbContext>(options => options.UseSqlServer(_configuration.GetConnectionString("SqlReleaseConnectionString")));
            }

            var jwtConfigOptions = _configuration.GetSection("JwtIssuerOptions");

            services.Configure<JwtIssuerOptions>(options =>
            {
                options.Issuer = jwtConfigOptions[nameof(JwtIssuerOptions.Issuer)];
                options.Audience = jwtConfigOptions[nameof(JwtIssuerOptions.Audience)];
                options.ValidFor = TimeSpan.FromMinutes(Double.Parse(jwtConfigOptions[nameof(JwtIssuerOptions.ValidFor)]));
                options.SigningCredentials = new SigningCredentials(_signingKey, SecurityAlgorithms.HmacSha256);
            });

            services.AddSingleton<IJwtFactory, JwtFactory>();
            services.TryAddTransient<IHttpContextAccessor, HttpContextAccessor>();

            // token validation
            var tokenValidationParameters = new TokenValidationParameters
            {
                ValidateIssuer = true,
                ValidIssuer = jwtConfigOptions[nameof(JwtIssuerOptions.Issuer)],

                ValidateAudience = true,
                ValidAudience = jwtConfigOptions[nameof(JwtIssuerOptions.Audience)],

                ValidateIssuerSigningKey = true,
                IssuerSigningKey = _signingKey,

                RequireExpirationTime = false,
                ValidateLifetime = true,
                ClockSkew = TimeSpan.Zero
            };

            services.AddAuthentication(options =>
            {
                options.DefaultAuthenticateScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultChallengeScheme = JwtBearerDefaults.AuthenticationScheme;
                options.DefaultScheme = JwtBearerDefaults.AuthenticationScheme;
            }).AddJwtBearer(config =>
            {
                config.ClaimsIssuer = jwtConfigOptions[nameof(JwtIssuerOptions.Issuer)];
                config.TokenValidationParameters = tokenValidationParameters;
                config.SaveToken = true;
            });

            services.AddAuthorization(options =>
            {
                options.AddPolicy(
                    "Admin",
                    policy => policy.RequireClaim(
                        JwtAppConstants.Strings.JwtClaimIdentifers.Rol,
                        JwtAppConstants.Strings.JwtClaims.Admin)
                        );
            });

            var builder = services.AddIdentityCore<AppUser>(options =>
            {
                //options.Password.RequireDigit = true;
                //options.Password.RequiredLength = 8;
                //options.Password.RequireLowercase = true;
                //options.Password.RequireUppercase = true;
                //options.Password.RequireNonAlphanumeric = true;

                options.Password.RequireDigit = false;
                options.Password.RequiredLength = 4;
                options.Password.RequireLowercase = false;
                options.Password.RequireUppercase = false;
                options.Password.RequireNonAlphanumeric = false;

                options.User.RequireUniqueEmail = true;
            });


            builder = new IdentityBuilder(builder.UserType, typeof(IdentityRole), builder.Services);
            builder.AddEntityFrameworkStores<AppDbContext>().AddDefaultTokenProviders();

            if (!_production)
            {
                services.AddCors(options =>
                {
                    options.AddPolicy(
                        "angular",
                        pol =>
                        {
                            pol.AllowAnyHeader()
                            .AllowAnyMethod()
                            .AllowAnyOrigin();
                        });
                });
            }
        }

        // This method gets called by the runtime. Use this method to configure the HTTP request pipeline.
        public void Configure(IApplicationBuilder app, IHostingEnvironment env)
        {
            if (env.IsDevelopment())
            {
                app.UseDeveloperExceptionPage();
            }

            app.UseForwardedHeaders(new ForwardedHeadersOptions
            {
                ForwardedHeaders = Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedFor | Microsoft.AspNetCore.HttpOverrides.ForwardedHeaders.XForwardedProto
            });

            if (!_production){
                app.UseCors("angular");
            }

            app.UseAuthentication();

            app.UseDefaultFiles();

            // create assets directory
            Directory.CreateDirectory(Directory.GetCurrentDirectory().ToString() + "/assets/");

            // create images directory
            Directory.CreateDirectory(Directory.GetCurrentDirectory().ToString() + "/assets" + "/images");

            app.UseStaticFiles(new StaticFileOptions
            {
                FileProvider = new PhysicalFileProvider(
                    Path.Combine(Directory.GetCurrentDirectory(), "assets")),
                RequestPath = "/assets"
            });

            app.UseMvc();

            //app.Use(async (context, next) =>
            //{
            //    await next();
            //    if (context.Response.StatusCode == 404 &&
            //             !Path.HasExtension(context.Request.Path.Value) &&
            //             !context.Request.Path.Value.StartsWith("/api/"))
            //    {
            //        context.Request.Path = "/index.html";
            //        await next();
            //    }
            //});
        }
    }
}

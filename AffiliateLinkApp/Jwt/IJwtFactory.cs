﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Security.Claims;
using System.Threading.Tasks;

namespace AffiliateLinkApp.Jwt
{
    public interface IJwtFactory
    {
        Task<string> GenerateEncodedToken(string userName, ClaimsIdentity claims);
        ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string rol);
    }
}

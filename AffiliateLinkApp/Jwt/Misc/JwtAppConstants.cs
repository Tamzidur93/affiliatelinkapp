﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AffiliateLinkApp.Jwt
{
    public static class JwtAppConstants
    {
        public static class Strings
        {
            public static class JwtClaimIdentifers
            {
                public const string Rol = "rol", id = "id";
            }

            public static class JwtClaims
            {
                public const string Admin = "admin";
            }
        }
    }
}

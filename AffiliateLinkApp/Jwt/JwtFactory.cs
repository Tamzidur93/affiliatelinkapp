﻿using Microsoft.Extensions.Options;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Principal;
using System.Threading.Tasks;

namespace AffiliateLinkApp.Jwt
{
    public class JwtFactory : IJwtFactory
    {
        private readonly JwtIssuerOptions _jwtOptions;

        public JwtFactory(IOptions<JwtIssuerOptions> options)
        {
            _jwtOptions = options.Value;
        }

        public ClaimsIdentity GenerateClaimsIdentity(string userName, string id, string rol)
        {
            return new ClaimsIdentity(
                new GenericIdentity(userName, "Token"),
                new[] {
                    new Claim(JwtAppConstants.Strings.JwtClaimIdentifers.id, id),
                    new Claim(JwtAppConstants.Strings.JwtClaimIdentifers.Rol, rol)
                });
        }

        public async Task<string> GenerateEncodedToken(string userName, ClaimsIdentity identity)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, userName),
                new Claim(JwtRegisteredClaimNames.Jti, await _jwtOptions.JtiGenerator()),
                new Claim(JwtRegisteredClaimNames.Iat, ToUnixEpochDate(_jwtOptions.IssuedAt).ToString(), ClaimValueTypes.Integer64),
                identity.FindFirst(JwtAppConstants.Strings.JwtClaimIdentifers.Rol),
                identity.FindFirst(JwtAppConstants.Strings.JwtClaimIdentifers.id)
            };

            var token = new JwtSecurityToken(
                issuer: _jwtOptions.Issuer,
                audience: _jwtOptions.Audience,
                claims: claims,
                notBefore: _jwtOptions.NotBefore,
                expires: _jwtOptions.Expiration,
                signingCredentials: _jwtOptions.SigningCredentials
                );

            var encodedToken = new JwtSecurityTokenHandler().WriteToken(token);

            return encodedToken;
        }

        private static long ToUnixEpochDate(DateTime date)
          => (long)Math.Round(
              (date.ToUniversalTime() - new DateTimeOffset(1970, 1, 1, 0, 0, 0, TimeSpan.Zero)).TotalSeconds);
    }
}
